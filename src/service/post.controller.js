const { response } = require('express');
const fs = require('fs');

import { postDao } from '../database/dao';
import { sequel } from '../database';


const toPost = async (req, res = response) => {
    console.log('[user.controller][toPost] start');

    const { idUser, post } = req.body;

    const t = await sequel.transaction();

    try {
        // create new Post
        const newPost = await postDao.toPost(post, t);

        // rel post and user
        await postDao.assignPostToUser(idUser, newPost.id, t);

        // save media of post
        for (const media of post.media) {
            const { name, type } = media;
            // create a path
            const path = `${process.env.POSTS_PATH}/${idUser}/${name}`;

            const newMedia = await postDao.saveMedia(path, type, t);

            // rel media and post
            await postDao.assignMediaToPost(newPost.id, newMedia.id, t);
        }

        t.commit();

        return res.status(200).json({
            message: 'OK'
        });
    } catch (error) {
        t.rollback();

        res.status(500).json({
            message: error.message,
            code: error.code || -1
        });
    }
}

const getUserPosts = async (req, res = response) => {
    console.log('[post.controller][getUserPosts] start');

    const { idUser } = req.params;
    const { page, size } = req.query;

    try {
        // get posts
        const posts = await postDao.getPostsUser(idUser, page, size);

        return res.status(200).json({
            message: 'OK',
            data: posts
        });
    } catch (error) {
        t.rollback();

        res.status(500).json({
            message: error.message,
            code: error.code || -1
        });
    }
}

const getFeedUser = async (req, res = response) => {
    console.log('[post.controller][getInboxUser] start');

    const { idUser } = req.params;
    const { page, size } = req.query;

    try {
        // get feed
        const feed = await postDao.getUserInbox(idUser, page, size);

        return res.status(200).json({
            message: 'OK',
            data: feed
        });
    } catch (error) {
        res.status(500).json({
            message: error.message,
            code: error.code || -1
        });
    }
}

const test = async (req, res = response) => {
    console.log("TEST");



}

export default {
    toPost,
    getUserPosts,
    getFeedUser,
    test
}