import { sequel } from '../database';

const { response } = require('express');
const bcrypt = require('bcryptjs');
const { userDao } = require('../database/dao');

const userSingup = async (req, res = response) => {
    console.log('[user.controller][userSingup] start');

    const { idCountry, email, password, username } = req.body;

    const t = await sequel.transaction();

    try {
        const user = {
            idCountry,
            email,
            password,
            username
        }

        // encrypt pass
        const hash = await bcrypt.hash(password, 10);

        // set user password to hash
        user.password = hash;

        // create new user
        const newUser = await userDao.singup(t, user);

        // rel role and user
        await userDao.assignUserRole(newUser.id, t);

        t.commit();

        return res.status(200).json({
            message: 'OK',
            data: newUser
        });
    } catch (error) {
        t.rollback();

        res.status(500).json({
            message: error.message,
            code: error.code || -1
        });
    }
}

const userFollow = async (req, res = response) => {
    console.log('[user.controller][userFollow] start');

    const { idUserFollowing, idUserFollower } = req.body;

    const t = await sequel.transaction();

    try {
        // create new follower
        await userDao.follow(idUserFollowing, idUserFollower, t);

        t.commit();

        return res.status(200).json({
            message: 'OK'
        });
    } catch (error) {
        t.rollback();

        res.status(500).json({
            message: error.message,
            code: error.code || -1
        });
    }
}

export default {
    userSingup,
    userFollow
}