import { jwtHelper } from '../helper';

const { response } = require('express');
const bcrypt = require('bcryptjs');

const { userDao, tempDao } = require('../database/dao');

const authUser = async (req, res = response) => {
    console.log('[auth.controller][authUser] start');

    const { email, password } = req.body;

    try {
        // validate if user exist
        const user = await userDao.findUserByEmail(email);
        if (!user) {
            return res.status(400).json({
                message: "El usuario o la contraseña no son validos"
            });
        }

        // verify password
        const validPassword = await bcrypt.compare(password, user.password);
        if (!validPassword) {
            return res.status(400).json({
                message: "El usuario o la contraseña no son validos"
            });
        }

        // generate token
        const token = await jwtHelper.generateJWT({ id: user.id, role: user.role });

        // // generate salt
        // const salt = bcrypt.genSaltSync();

        // // encrypt token
        // const tokenHash = bcrypt.hashSync(token, salt);

        // console.log(`token: ${token} --- salt: ${salt} --- tokenHash: ${tokenHash}`);
        // // save temp jwt
        // await tempDao.registerTempToken({
        //     idUser: user.id,
        //     tempHash: tokenHash
        // });

        return res.status(200).json({
            message: 'OK',
            data: {
                token
            }
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: error.message,
            code: error.code || -1
        });
    }
}

export default {
    authUser
}