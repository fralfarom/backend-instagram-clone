import userController from './user.controller';
import profileController from './profile.controller';

export {
    userController,
    profileController
}

export default {
    userController,
    profileController
}