import userDao from '../database/dao/user.dao';

const { response } = require('express');

const getProfileInfo = async (req, res = response) => {
    console.log('[profile.controller][getProfileInfo] start');

    const { idUser } = req.params;

    try {
        const profile = await userDao.getProfileInfo(idUser);

        res.status(200).json({
            message: 'OK',
            data: profile
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: error.message,
            code: error.code || -1
        });
    }
}

export default {
    getProfileInfo
}