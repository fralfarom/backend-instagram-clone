const jwt = require('jsonwebtoken');

export const generateJWT = (sessionInfo) => {
    return new Promise((resolve, reject) => {
        const payload = { ...sessionInfo };

        jwt.sign(payload, process.env.SECRET_JWT, {
            expiresIn: '4h'
        }, (err, token) => {
            if (err) {
                console.log(err);
                reject('No se pudo generar el token');
            } else {
                resolve(token);
            }
        });
    });
}