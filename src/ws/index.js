import mtdWs from "./mtd-ws";
import cuentaEjecucionWs from "./cuentaEjecucion-ws";
import documentoWs from "./documento-ws";

export {
    mtdWs,
    cuentaEjecucionWs,
    documentoWs
}

export default {
    mtdWs,
    cuentaEjecucionWs,
    documentoWs
}