import express from 'express';

import healthRoute from './health-route';
import userRoute from './user.routes';
import authRoute from './auth.routes';
import profileRoute from './profile.routes';
import postRoute from './post.routes';

const app = express()

app.use(
    healthRoute,
    userRoute,
    authRoute,
    profileRoute,
    postRoute
)

export default app;