import express from 'express';
import { userController } from '../service';

const app = express();

const router = express.Router();

app.use('/user', router);

router.post('/', userController.userSingup);

router.post('/follow', userController.userFollow);

export default app;