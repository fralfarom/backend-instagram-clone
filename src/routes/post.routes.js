import express from 'express';
import { destinePosts } from '../middlewares/multer';
import postController from '../service/post.controller';

const app = express();

const router = express.Router();

app.use('/post', router);

router.post('/', [destinePosts], postController.toPost);

router.get('/user/:idUser', postController.getUserPosts);

router.get('/feed/:idUser', postController.getFeedUser);

router.post('/test', [destinePosts], postController.test);

export default app;