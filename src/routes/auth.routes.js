import express from 'express';
import authController from '../service/auth.controller';

const app = express();

const router = express.Router();

app.use('/auth', router);

router.post('/user', authController.authUser);

export default app;