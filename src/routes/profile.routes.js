import { profileController } from '../service';

const express = require('express');
const { validateJWT } = require('../middlewares/validate-jwt');

const app = express();

const router = express.Router();

app.use('/profile', router);

router.get('/:idUser', [validateJWT], profileController.getProfileInfo);

export default app;