import axios from "axios";

export const agregarParametroActualizar = (param, text) => `${param && text || ''}`;

export const sanitizarUpdate = text => text.trim().replace(/\,$/g, '');

export default {
    agregarParametroActualizar,
    sanitizarUpdate,
    toBase64
}