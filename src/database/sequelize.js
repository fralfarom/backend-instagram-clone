import { database as dbConfig } from '../config'
import { Sequelize } from 'sequelize'
import SecretManager from '../config/secret-manager';

export let sequelize

if (process.env.NODE_ENV === 'local') {
    sequelize = new Sequelize(dbConfig)
} else {
    sequelize = new Sequelize({
        dialect: 'postgres',
        timezone: '-03:00',
        pool: {
            max: Number(process.env.POOL_MAX) || 5,
            min: Number(process.env.POOL_MIN) || 0,
            idle: Number(process.env.POOL_IDLE) || 30000,
            acquire: Number(process.env.POOL_ACQUIRE) || 1000
          }        
    })
    sequelize.beforeConnect(async (config) => {        
        const secret = JSON.parse(await SecretManager.getSecret())
        console.log(`USER = ${secret.username}`)
        config.host = process.env.RUN_LOCAL === 'true' ? 'host.docker.internal' : secret.host
        config.username = secret.username
        config.password = secret.password
        config.database = secret.dbname
        config.port = secret.port
    })
}

sequelize.afterConnect(async (conn, _opt) => {
    const searchPath = process.env.SCHEMA
    console.log(`Seteando esquema ${searchPath}... `)
    try {
        await conn.query(`SET search_path = ${searchPath} `)
    } catch (err) {
        console.error('Error seteando esquema', err)
    }
})

export { Sequelize }

export default { sequelize, Sequelize }