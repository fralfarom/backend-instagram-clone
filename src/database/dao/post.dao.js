import { sequel } from "../";
import { QueryTypes } from 'sequelize';

const msgNoPost = 'the post was not found';
const codeNoPost = -4;

export const toPost = async (post, t = null) => {
    console.log('[post.dao][toPost] start');

    let idResult;
    let affectedResult;

    try {
        const queryStr = `insert into tal_post (    id_post_type, 
                                                    description) 
                                        values  (   $idPostType, 
                                                    $description) 
                                        returning id
                        `;

        const params = { ...post };
        console.log("PARAMS: ", params);

        const [id, affected] = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.INSERT,
            transaction: t
        });

        idResult = id;
        affectedResult = affected;

    } catch (err) {
        console.error('Error at to post', err);
        const error = Error(`Error at to post ${err.message}`);
        error.code = -1;
        throw error;
    }

    if (!affectedResult) {
        console.log('The post was not published');
        const err = Error('The post was not published');
        err.code = -1;
        throw err;
    }

    return idResult[0];
}

export const assignPostToUser = async (idUser, idPost, t = null) => {
    console.log('[post.dao][assignPostToUser] start');

    let idResult;
    let affectedResult;

    try {
        const queryStr = `insert into rel_user_post (   id_user, 
                                                        id_post) 
                                            values  (   $idUser, 
                                                        $idPost) 
                                            returning id
                        `;

        const params = { idUser, idPost };
        console.log("PARAMS: ", params);

        const [id, affected] = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.INSERT,
            transaction: t
        });

        idResult = id;
        affectedResult = affected;

    } catch (err) {
        console.error('Error at assign post to user', err);
        const error = Error(`Error at assign post to user ${err.message}`);
        error.code = -1;
        throw error;
    }

    if (!affectedResult) {
        console.log('The post was not assigned to user');
        const err = Error('The post was not assigned to user');
        err.code = -1;
        throw err;
    }

    return idResult[0];
}

export const getPostsUser = async (idUser, page = 1, size = 10) => {
    console.log('[post.dao][getPostsUser] start');

    const ID_POST_TYPE = 1;

    let results;

    try {
        const queryStr = `  select  tp.id,
                                    tp.description,
                                    count(rpul.id) as "likes"
                            from "general".tal_post tp
                            inner join "general".rel_user_post rup on (tp.id = rup.id_post)
                            left join "general".rel_post_user_like rpul on (tp.id = rpul.id_post)
                            where rup.id_user = $idUser
                            and tp.id_post_type = $idPostType
                            group by tp.id,	tp.description
                            limit $size
                            offset ($page - 1) * 10
                        `;

        const params = { idUser, idPostType: ID_POST_TYPE, size, page };
        console.log("PARAMS: ", params);

        results = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.SELECT
        });

    } catch (err) {
        console.error('[getPostsUser] Error at find user posts', err);
        const error = Error(` Error at find user posts: ${err.message}`);
        error.code = -6;
        throw error;
    }

    if (!results) {
        console.log(msgNoPost);
        const err = Error(msgNoPost);
        err.code = codeNoPost;
        throw err;
    }

    console.log("RESULTS: ", results);
    return results;
}

export const getUserInbox = async (idUser, page = 1, size = 10) => {
    console.log('[post.dao][getUserInbox] start');

    const ID_POST_TYPE = 1;

    let results;

    try {
        const queryStr = `  select  tp.id, 
                                    tp.description,
                                    tp.reg_date as "date"
                            from tal_user tu 
                            inner join rel_user_follow ruf on (tu.id = ruf.id_user_follower)
                            inner join rel_user_post rup on (ruf.id_user_following = rup.id_user or tu.id = rup.id_user)
                            inner join tal_post tp on (rup.id_post = tp.id)
                            where tu.id = $idUser
                            and tp.id_post_type = $idPostType
                            group by tp.id,	tp.description
                            order by tp.reg_date desc
                            limit $size
                            offset ($page - 1) * 10
                        `;

        const params = { idUser, idPostType: ID_POST_TYPE, size, page };
        console.log("PARAMS: ", params);

        results = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.SELECT
        });

    } catch (err) {
        console.error('[getUserInbox] Error at get feed of user', err);
        const error = Error(` Error at get feed of user: ${err.message}`);
        error.code = -6;
        throw error;
    }

    if (!results) {
        console.log(msgNoPost);
        const err = Error(msgNoPost);
        err.code = codeNoPost;
        throw err;
    }

    console.log("RESULTS: ", results);
    return results;
}

export const saveMedia = async (path, type, t = null) => {
    console.log('[post.dao][saveMedia] start');

    let idResult;
    let affectedResult;

    try {
        const queryStr = `insert into tal_media (   path, 
                                                    type) 
                                        values  (   $path, 
                                                    $type) 
                                        returning id
                        `;

        const params = { path, type };
        console.log("PARAMS: ", params);

        const [id, affected] = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.INSERT,
            transaction: t
        });

        idResult = id;
        affectedResult = affected;

    } catch (err) {
        console.error('Error at save media', err);
        const error = Error(`Error at save media ${err.message}`);
        error.code = -1;
        throw error;
    }

    if (!affectedResult) {
        console.log('The media was not saved');
        const err = Error('The media was not saved');
        err.code = -1;
        throw err;
    }

    return idResult[0];
}

export const assignMediaToPost = async (idPost, idMedia, t = null) => {
    console.log('[post.dao][assignMediaToPost] start');

    let idResult;
    let affectedResult;

    try {
        const queryStr = `insert into rel_media_post (  id_media, 
                                                        id_post) 
                                            values  (   $idMedia, 
                                                        $idPost) 
                                            returning id
                        `;

        const params = { idPost, idMedia };
        console.log("PARAMS: ", params);

        const [id, affected] = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.INSERT,
            transaction: t
        });

        idResult = id;
        affectedResult = affected;

    } catch (err) {
        console.error('Error at assign media to post', err);
        const error = Error(`Error at assign media to post ${err.message}`);
        error.code = -1;
        throw error;
    }

    if (!affectedResult) {
        console.log('The media was not assigned to post');
        const err = Error('The media was not assigned to post');
        err.code = -1;
        throw err;
    }

    return idResult[0];
}

export default {
    toPost,
    assignPostToUser,
    getPostsUser,
    getUserInbox,
    saveMedia,
    assignMediaToPost
}