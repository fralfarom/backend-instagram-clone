import { sequel } from "../";
import { QueryTypes } from 'sequelize';

const msgNoUser = 'the user was not found';
const codeNoUser = -4;

export const singup = async (t, user) => {
    console.log('[user.dao][signup] start');

    let idResult;
    let affectedResult;

    try {
        const queryStr = `insert into tal_user (    id_country, 
                                                    email, 
                                                    password,
                                                    username) 
                                        values  (   $idCountry, 
                                                    $email, 
                                                    $password,
                                                    $username) 
                                        returning id
                        `;

        const params = { ...user };
        console.log("PARAMS: ", params);

        const [id, affected] = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.INSERT,
            transaction: t
        });

        idResult = id;
        affectedResult = affected;

    } catch (err) {
        console.error('Error at singup user', err);
        const error = Error(`Error at singup user ${err.message}`);
        error.code = -1;
        throw error;
    }

    if (!affectedResult) {
        console.log('User is not registered');
        const err = Error('User is not registered');
        err.code = -1;
        throw err;
    }

    return idResult[0];
}

export const findUserByEmail = async (email) => {
    console.log('[user.dao][findUserByEmail] start');

    let results;

    try {
        const queryStr = `  select  tu.id, 
                                    tu.password, 
                                    tu.email,
                                    rru.id_role as "role"
                            from tal_user tu
                            inner join rel_role_user rru on (tu.id = rru.id_user)
                            where email = $email
                        `;

        const params = { email };
        console.log("PARAMS: ", params);

        results = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.SELECT
        });

    } catch (err) {
        console.error('[findUserByEmail] Error at find user by email', err);
        const error = Error(` Error at find user by email: ${err.message}`);
        error.code = -6;
        throw error;
    }

    if (!results) {
        console.log(msgNoUser);
        const err = Error(msgNoUser);
        err.code = codeNoUser;
        throw err;
    }

    return results[0];
}

export const findUserById = async (id) => {
    console.log('[user.dao][findUserById] start');

    let results;

    try {
        const queryStr = `  select  id
                            from tal_user 
                            where id = $id
                        `;

        const params = { id };
        console.log("PARAMS: ", params);

        results = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.SELECT
        });

    } catch (err) {
        console.error('[findUserById] Error at find user by id', err);
        const error = Error(` Error at find user by id: ${err.message}`);
        error.code = -6;
        throw error;
    }

    if (!results) {
        console.log(msgNoUser);
        const err = Error(msgNoUser);
        err.code = codeNoUser;
        throw err;
    }

    console.log("RESULTS: ", results);
    return results[0];
}

export const getProfileInfo = async (id) => {
    console.log('[user.dao][getProfileInfo] start');

    let results;

    try {
        const queryStr = `  select  tu.id,
                            count(rup.id_post) as posts,
                            count(distinct (ruf.id)) as followers,
                            count(distinct (ruf2.id)) as followings 
                            from general.tal_user tu
                            left outer join general.rel_user_post rup on(rup.id_user=tu.id)
                            left outer join general.rel_user_follow ruf on(ruf.id_user_following=tu.id)
                            left outer join general.rel_user_follow ruf2 on(ruf2.id_user_follower=tu.id )
                            group by tu.id
                        `;

        const params = { id };
        console.log("PARAMS: ", params);

        results = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.SELECT
        });

    } catch (err) {
        console.error('[findUserById] Error at find user by id', err);
        const error = Error(` Error at find user by id: ${err.message}`);
        error.code = -6;
        throw error;
    }

    if (!results) {
        console.log(msgNoUser);
        const err = Error(msgNoUser);
        err.code = codeNoUser;
        throw err;
    }

    console.log("RESULTS: ", results);
    return results[0];
}

export const assignUserRole = async (idUser, t = null) => {
    console.log('[user.dao][assignUserRole] start');

    const ID_USER_ROLE = 1;

    let idResult;
    let affectedResult;

    try {
        const queryStr = `insert into rel_role_user (   id_role, 
                                                        id_user) 
                                            values  (   $idRole, 
                                                        $idUser) 
                                            returning id
                        `;

        const params = { idRole: ID_USER_ROLE, idUser };
        console.log("PARAMS: ", params);

        const [id, affected] = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.INSERT,
            transaction: t
        });

        idResult = id;
        affectedResult = affected;

    } catch (err) {
        console.error('Error at assignUserRole', err);
        const error = Error(`Error at assignUserRole ${err.message}`);
        error.code = -1;
        throw error;
    }

    if (!affectedResult) {
        console.log('Role was not assigned to user');
        const err = Error('Role was not assigned to user');
        err.code = -1;
        throw err;
    }

    return idResult[0];
}

export const follow = async (idUserFollowing, idUserFollower, t = null) => {
    console.log('[user.dao][follow] start');

    let idResult;
    let affectedResult;

    try {
        const queryStr = `insert into rel_user_follow   (   id_user_following, 
                                                            id_user_follower) 
                                                values  (   $idUserFollowing, 
                                                            $idUserFollower) 
                                                returning id
                        `;

        const params = { idUserFollowing, idUserFollower };
        console.log("PARAMS: ", params);

        const [id, affected] = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.INSERT,
            transaction: t
        });

        idResult = id;
        affectedResult = affected;

    } catch (err) {
        console.error('Error at follow user', err);
        const error = Error(`Error at follow user ${err.message}`);
        error.code = -1;
        throw error;
    }

    if (!affectedResult) {
        console.log('User was not followed');
        const err = Error('User was not followed');
        err.code = -1;
        throw err;
    }

    return idResult[0];
}

export default {
    singup,
    findUserByEmail,
    findUserById,
    getProfileInfo,
    assignUserRole,
    follow
}