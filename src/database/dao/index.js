import userDao from './user.dao';
import tempDao from './temp.dao';
import postDao from './post.dao';

export {
    userDao,
    tempDao,
    postDao
}

export default {
    userDao,
    tempDao,
    postDao
}