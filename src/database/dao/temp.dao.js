import { QueryTypes } from "sequelize";
import { sequel } from "..";


export const registerTempToken = async (temp, t = null) => {
    console.log('[temp.dao][registerTempToken] start');

    let affectedResult;

    try {
        const queryStr = `insert into tal_temp_token    (   id_user, 
                                                            temp_hash)
                                                values  (   $idUser,
                                                            $tempHash)
                        `;

        const params = { ...temp };
        console.log("params: ", params);

        const [affected] = await sequel.query(queryStr, {
            bind: params,
            type: QueryTypes.INSERT,
            transaction: t
        });

        affectedResult = affected;
    } catch (err) {
        console.error('Error at save JWT', err);
        const error = Error(`Error at save JWT ${err.message}`);
        error.code = -1;
        throw error;
    }

    if (!affectedResult) {
        console.log('JWT is not registered');
        const err = Error('JWT is not registered');
        err.code = -1;
        throw err;
    }
}

export default {
    registerTempToken,
}