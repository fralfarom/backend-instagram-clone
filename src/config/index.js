import './env'
import database from './database'
import { getSecret } from './secret-manager'

export {
    database,
    getSecret
}

export default {
    database,
    getSecret
}