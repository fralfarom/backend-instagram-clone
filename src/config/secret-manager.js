import SecretsManager from 'aws-sdk/clients/secretsmanager'
const secretName = process.env.SECRET_NAME || 'dev/ine/secrets'
const region = process.env.REGION || 'us-west-2'

const client = new SecretsManager({ 
    region
 })

export const getSecret = async () => {

    const response = await client.getSecretValue({ SecretId: secretName }).promise()

    if ('SecretString' in response) {
        return response.SecretString
    }

    return  Buffer.from(response.SecretBinary, "base64").toString("ascii")

}

export default { getSecret }