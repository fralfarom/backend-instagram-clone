const multer = require('multer');
const path = require('path');

const destinePosts = async (req, res, next) => {
    const { idUser } = req.body;

    multer({
        dest: path.join(`${process.env.POSTS_PATH}/${idUser}/`)
    }).array('media', 10);

    next();
}

module.exports = {
    destinePosts
}