const jwt = require('jsonwebtoken');

const { userDao } = require('../database/dao');

const validateJWT = async (req, res, next) => {
    const token = req.header('x-token');

    if (!token) {
        return res.status(401).json({
            message: "No hey token en la peticion"
        });
    }

    try {
        const { id } = jwt.verify(token, process.env.SECRET_JWT);

        // read user of DB
        const user = await userDao.findUserById(id);

        // verify if user exist
        if (!user) {
            return res.status(401).json({
                message: "Token no valido"
            });
        }

        next();
    } catch (error) {
        console.log("catch error: ", error);
        return res.status(401).json({
            message: error.message
        });
    }
}

module.exports = {
    validateJWT
}